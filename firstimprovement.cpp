#include "firstimprovement.h"

vector<int> FirstImprovement::findFirstImprovement(vector<vector<int> > n, vector<int> current)
{
    long int currentWCT = this->getInstance()->newComputeWCT(current);
    for(vector<int> v: n){
        long int newWCT = this->getInstance()->newComputeWCT(v);
        if(newWCT < currentWCT)
            return v;
    }

    return {};
}

vector<vector<int> > FirstImprovement::generatePartialNeighbourhood(vector<int> current, int fromwhere)
{
    if(fromwhere == 0)
        return this->getNeighbourHood()->generateNeighbourHood(current);

    vector<int> part(current.begin(),current.begin()+fromwhere);
    vector<int> tocheck(current.begin()+fromwhere, current.end());
    vector<vector<int>> retval;
    vector<vector<int>> nbourtocheck = this->getNeighbourHood()->generateNeighbourHood(tocheck);

    for(vector<int> nbour: nbourtocheck){
        nbour.insert(nbour.begin(),part.begin(),part.end());
        retval.push_back(nbour);
    }

    return retval;

}

FirstImprovement::FirstImprovement(PfspInstance *instance, NeighbourHood *hood, vector<int> startpoint)
    :PivotRule(instance,hood,NULL)
{
    this->startpoint = startpoint;
}

FirstImprovement::FirstImprovement(PfspInstance* instance, NeighbourHood* hood,InitialSolution *initsol)
    :PivotRule(instance,hood,initsol)
{

}

vector<int> FirstImprovement::searchForSolution(int timelimit)
{
    timer.start();

    vector<int> pi;
    if(this->getInitialSolution() == NULL) {
        pi = startpoint;
    } else {
        pi = this->getInitialSolution()->generateInitialSolution();
    }

    for(;;) {
        if(timelimit > 0) {
            quint64 elapsed = timer.elapsed();
            if(elapsed > timelimit)
                break;
        }
        for(int i = 0; i < this->getInstance()->getNbJob(); i++) {
            vector<vector<int>> N_pi = this->generatePartialNeighbourhood(pi,i);
            vector<int> tocheck = findFirstImprovement(N_pi,pi);
            if(tocheck.empty())
                break;
            else
                pi = tocheck;
        }
        vector<vector<int>> newneigh = this->getNeighbourHood()->generateNeighbourHood(pi);
        vector<int> imp = findFirstImprovement(newneigh,pi);
        if(imp.empty())
            break;
        else
            pi = imp;
    }
    return pi;
}
