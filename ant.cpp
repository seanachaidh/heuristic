#include "ant.h"

Ant::Ant(AntColony *colony, vector<int> initsol)
{
    this->colony = colony;
    this->my_solution = initsol;
}

void Ant::buildSolution()
{
    int nbjob = this->colony->getInstance()->getNbJob();
    vector<int> so_far = colony->getBest();
    vector<int> sol;

//    cout << "So far: " << endl;
//    for(int i: so_far) cout << i << ",";
//    cout << endl;

    for(int i = 0; i < nbjob; i++) {
        //take a number between 0 and 1
        //cout << "building step" << endl;
        float u = ((float) rand() / RAND_MAX);
        if(u <= 0.4) {
            sol.push_back(so_far.front());
            //kostelijke operatie
            so_far.erase(so_far.begin());
        } else {
            if(u <= 0.8) {
                //int T_ik = 0;
                 vector<pair<int,double>>T_ik = colony->makeTik(so_far);
                 vector<pair<int,double>> tmp = copy_at_least(5,T_ik);
                 sol.push_back(tmp.front().first);
                 so_far = removeJob(tmp.front().first,so_far); //Remove the job
            } else {
                vector<int> part = copy_at_least(5,so_far);
                vector<pair<int,double>>tmp = colony->makeTik(part);
                double sumtk = 0;
                for(pair<int,double> p: tmp) sumtk+= p.second;
                //change the tk values in probablities. Same vector is used to save some memory space
                //for(pair<int,double> p: tmp) p.second = p.second/sumtk;
                vector<pair<int,double>> norm = normalise(tmp,sumtk);
                int j = chooseRandomJob(norm);
                so_far = removeJob(j,so_far);
                sol.push_back(j);
            }
        }
    }
    //Wat hiermee te doen?

    //cout << "Length sol: " << sol.size() << endl;
    //cout << "Number jobs: " << nbjob << endl;
    this->my_solution = sol;
}


//TODO: Nog nakijken of dit echt klopt!
void Ant::updatePheromone()
{
//    long mygoal = this->colony->getInstance()->newComputeWCT(my_solution);
    int njobs = this->colony->getInstance()->getNbJob();
    for(int i = 0; i < njobs; i++){
        for(int k = 0; k < njobs; k++) {
            double evap = this->colony->pheromone[i][k] * this->colony->rho;
            if(this->my_solution[k] == i) {
                int posjob = findJob(i,this->colony->getBest());
                double diff = sqrt(abs(posjob - k) + 1);
                 this->colony->pheromone[i][k] = evap + diff*100;
            } else {
                this->colony->pheromone[i][k] = evap;
            }
        }
    }
}


bool Ant::is_element(int i, vector<int> li)
{
    bool retval = false;
    for(int j: li) {
        if(j == i)
            retval = true;
    }

    return retval;
}


vector<pair<int, double> > Ant::copy_at_least(int n, vector<pair<int, double> > li)
{
    vector<pair<int,double>> retval;
    vector<pair<int,double>>::iterator itt = li.begin();

    while(!(n == 0) && !(itt == li.end()))
    {
        retval.push_back(*itt);
        itt++;n--;
    }

    return retval;
}

vector<int> Ant::copy_at_least(int n, vector<int> li)
{
    vector<int> retval;
    vector<int>::iterator itt = li.begin();

    while(!(n == 0) && !(itt == li.end()))
    {
        retval.push_back(*itt);
        itt++;n--;
    }

    return retval;
}

vector<int> Ant::getSolution()
{
    return this->my_solution;
}

vector<pair<int, double> > Ant::normalise(vector<pair<int, double> > l,double sum)
{
    vector<pair<int,double>> retval;
    for(pair<int,double> p: l){
        retval.push_back(make_pair(p.first,(p.second/sum)));
    }

    return retval;
}

int Ant::chooseRandomJob(vector<pair<int,double>> jobs)
{
    double r = ((double) rand() /RAND_MAX);
    double counter = 0;
    for(unsigned int i = 0; i < jobs.size(); i++){
         counter += jobs[i].second;
         if(counter >= r){
             return jobs[i].first;
         }
    }
    return 0;
}

vector<int> Ant::removeJob(int i, vector<int> jobs)
{
    for(unsigned int j = 0; j < jobs.size(); j++){
        if (jobs[j] == i) jobs.erase(jobs.begin() + j);
    }
    return jobs;
}

int Ant::findJob(int i, vector<int> jobs)
{
    for(long j = 0; j < jobs.size(); j++){
        if(jobs[j] == i)
            return j;

    }

    return -1;
}


