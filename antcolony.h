#ifndef ANTCONOLY_H
#define ANTCONOLY_H

#include "mainheader.h"
#include <QElapsedTimer>

class Ant;

class AntColony: public PivotRule
{
private:
    int maxiterations;

    vector<Ant*> ants;

    int timelimit;
    QElapsedTimer timer;

    //Best found solution
    vector<int> found_solution;
    int solutiongoal;

    //private methods
    void initialisePheromone();
    bool shouldTerminate();

    double caclulate_sum(vector<double> li);

public:
    vector<vector<double>> pheromone;

    //Werkt dit wel?
    const double rho;

    AntColony(PfspInstance* instance, int numants, int maxiterations, double rho);
    virtual vector<int> searchForSolution(int timelimit = -1);

    vector<int> getBest();

    vector<pair<int, double> > makeTik(vector<int> jobsdone);
    static bool sort_comp(pair<int,double> x, pair<int,double> y);

    //testje
    //void setPheromone(int i, int j, double val);
};

#endif // ANTCONOLY_H
