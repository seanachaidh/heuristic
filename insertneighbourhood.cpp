#include "insertneighbourhood.h"

vector<int> InsertNeighbourHood::performInsert(vector<int> vec, int x, int y)
{
    vector<int> retval = vec;
    int tmp = retval[x];
    retval.erase(retval.begin() + x);
    retval.insert(retval.begin() + y, tmp);

    return retval;
}

InsertNeighbourHood::InsertNeighbourHood(PfspInstance *instance)
    :NeighbourHood(instance)
{

}

//TODO: Dit moet grondig getest worden
vector<vector<int> > InsertNeighbourHood::generateNeighbourHood(vector<int> state)
{
    vector<vector<int>> retval;

    for(unsigned int i = 0; i < state.size(); i++) {
        vector<int> tmpvec = state;
        int tmpint = state[i];
        tmpvec.erase(tmpvec.begin() + i);
        for(unsigned int j = 0; j < state.size(); j++){
            if(i != j) {
                vector<int> finalvec = tmpvec;
                finalvec.insert(finalvec.begin() + j, tmpint);
                retval.push_back(finalvec);
            }
        }
    }

    return retval;
}
