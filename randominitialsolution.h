#ifndef RANDOMINITIALSOLUTION_H
#define RANDOMINITIALSOLUTION_H

#include <algorithm>

#include "initialsolution.h"
/**
 * @brief The RandomInitialSolution class
 * This class is an implementation of InitialSolution that can generate
 * random initial solutions.
 */
class RandomInitialSolution : public InitialSolution
{
public:
    RandomInitialSolution(PfspInstance *instance);
    /**
     * @brief generateInitialSolution
     * @return Initial solution
     *
     * Generates a random initial solution for an instance. That is, a random
     * permutation of the jobs in the insrance.
     *
     */
    virtual vector<int> generateInitialSolution();
    virtual string toString() {return "RND";}
};

#endif // RANDOMINITIALSOLUTION_H
