#ifndef ANT_H
#define ANT_H

#include "mainheader.h"

class AntColony;

class Ant
{

private:
    vector<int> my_solution;
    AntColony* colony;

public:
    Ant(AntColony *colony, vector<int> initsol);
    void buildSolution();

    void updatePheromone();

    //helping methods
    bool is_element(int i,vector<int>li);

    vector<int> removeJob(int i, vector<int>jobs);
    int findJob(int i, vector<int> jobs);
    int chooseRandomJob(vector<pair<int, double> > jobs);

    vector<pair<int,double>> copy_at_least(int n, vector<pair<int,double>> li);
    vector<int> copy_at_least(int n, vector<int> li);


    vector<int> getSolution();
    vector<pair<int,double>> normalise(vector<pair<int,double>> l, double sum);

};

#endif // ANT_H
