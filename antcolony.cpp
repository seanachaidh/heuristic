#include "antcolony.h"
void AntColony::initialisePheromone()
{
    int pmatsize = (this->getInstance()->getNbJob())*(this->getInstance()->getNbJob());
    for(int i = 0; i < pmatsize; i++) {
        vector<double> tmpvec;
        for(int j = 0; j < pmatsize; j++) {
//            tmpvec.push_back(1/solutiongoal);
            //report this
            tmpvec.push_back(1);
        }
        pheromone.push_back(tmpvec);
    }
}

bool AntColony::shouldTerminate()
{
    quint64 elapsed = timer.elapsed();
    if((elapsed) > timelimit)
        return true;
    else
        return false;
}

double AntColony::caclulate_sum(vector<double> li)
{
    double retval = 0;
    for(double d: li){
        retval += d;
    }
    return retval;
}


vector<pair<int, double> > AntColony::makeTik(vector<int> jobsdone)
{
    vector<pair<int,double>> retval;
    int nbjobs = this->getInstance()->getNbJob();
    for(int i = 0; i<nbjobs; i++){
        if(find(jobsdone.begin(),jobsdone.end(),i) != jobsdone.end()){
            double tik = this->caclulate_sum(this->pheromone[i]);
            retval.push_back(make_pair(i,tik));
        }
    }

    //We sort it allready
    sort(retval.begin(),retval.end(),sort_comp);
    return retval;
}


bool AntColony::sort_comp(pair<int, double> x, pair<int, double> y)
{
    //TODO Nog eens nakijken of dit de elementen in dalende volgorde teruggeeft
    return x.second > y.second;
}



AntColony::AntColony(PfspInstance *instance, int numants, int maxiterations,double rho)
    :PivotRule(instance, new InsertNeighbourHood(instance),new RZPermutation(instance)), rho(rho)
{
    this->found_solution = this->getInitialSolution()->generateInitialSolution();
    this->solutiongoal = this->getInstance()->newComputeWCT(this->found_solution);
    this->maxiterations = maxiterations;

    for(int i = 0; i < numants; i++){
        ants.push_back(new Ant(this, found_solution));
    }

}

vector<int> AntColony::searchForSolution(int timelimit)
{

    //TODO: Should remove the iteration criteria since this is no longer used in function shouldTerminate
    int iter = 0;

    this->timelimit = timelimit;
    timer.start();

    initialisePheromone();
    while(!shouldTerminate()){
        for(Ant* a: this->ants){
            a->buildSolution();
            FirstImprovement* improver = new FirstImprovement(this->getInstance(),this->getNeighbourHood(),a->getSolution());
            int time_to_improve = timelimit - timer.elapsed();
            if(time_to_improve < 0) break;
            vector<int> improved_solution = improver->searchForSolution(time_to_improve);

            long int checksol = this->getInstance()->newComputeWCT(improved_solution);

            //This ant found a better solution!
            if ((checksol < solutiongoal) && (improved_solution.size() != 0)) {
                solutiongoal = checksol;
                found_solution = improved_solution;
            }

        }

        for(Ant* a: this->ants)
            a->updatePheromone();
        iter++;
    }

    return found_solution;
}

vector<int> AntColony::getBest()
{
    return this->found_solution;
}
