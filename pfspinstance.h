/***************************************************************************
 *   Copyright (C) 2012 by Jérémie Dubois-Lacoste   *
 *   jeremie.dl@gmail.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef _PFSPINSTANCEWT_H_
#define _PFSPINSTANCEWT_H_

#include <string>
#include <vector>
#include <tuple>
#include <algorithm>
#include <map>

//Eigen headers
#include "helpers.h"

using namespace std;

/**
 * @brief The PfspInstance class
 * This class represents an instance and contains the processing matrix for an instance
 */
class PfspInstance{
  private:
    int nbJob;
    int nbMac;
    std::vector< long int > dueDates;
    std::vector< long int > priority;

    map<string, long> memoise;

    std::vector< std::vector <long int> > processingTimesMatrix;

  public:
    PfspInstance();
    PfspInstance(vector<vector<long int>> matrix, vector<int> priority);
    ~PfspInstance();

    /* Read write privates attributs : */
    int getNbJob();
    int getNbMac();

    /* Allow the memory for the processing times matrix : */
    void allowMatrixMemory(int nbJ, int nbM);

    /* Read\Write values in the matrix : */
    long int getTime(int job, int machine);
    void setTime(int job, int machine, long int processTime);

    long int getDueDate(int job);
    void setDueDate(int job, int value);

    long int getPriority(int job);
    void setPriority(int job, int value);

    /* Read Data from a file : */
    /**
     * @brief readDataFromFile
     * @param fileName The file to be loaded
     * @return True if the file is correctly loaded, False otherwise
     *
     * Loads an instance from a file.
     *
     */
    bool readDataFromFile(const char * fileName);

    /**
     * @brief computeWCT
     * @deprecated Do not use!
     * @param sol
     * @return the evaluation function
     */
    long int computeWCT (vector< int > & sol);
    /**
     * @brief newComputeWCT
     * @param sol A permutation of jobs representing a solution
     * @return The value of the evalution function for a given permutation of jobs
     *
     * Computes the evaluation function for a given permutiation of jobs from the instance
     * This can be a partial solution
     *
     */
    long int newComputeWCT(vector<int> sol);

    //Permutations
    vector<int> randomPermutation();

};

#endif
