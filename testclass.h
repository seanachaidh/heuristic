#ifndef TESTCLASS_H
#define TESTCLASS_H

#include <fstream>

#include <QObject>
#include <QtTest/QtTest>

//Ik weet niet zeker of dit wel portable is
#include <QDir>

//Voor de tijd te meten
#include <QElapsedTimer>

#include "mainheader.h"

class TestClass : public QObject
{
    Q_OBJECT

public:
    TestClass(int limit, bool vnd, bool changeup);
private:
    PfspInstance* testInstance;
    PfspInstance* secondInstance;
    QStringList instancefiles;

    PfspInstance* loadFile(QString filename);

    PivotRule* createTestInstance(Pivot pivot,Neighbour neighbour, Solution solution, QString filename);
    PivotRule *createTestInstance(Pivot pivot,Solution solution,QString filename);

    int limit;
    bool vnd;
    bool changeup;

    vector<Neighbour> neighbours = {EXCHANGE,INSERT,TRANSPOSE};
    vector<Pivot> pivots = {BEST,FIRST};
    vector<Solution> solutions = {SIMPLIFIED,RANDOM};

private slots:
    void initTestCase();
    void testInsertNeighbourHood();
    void testRZHeuristic();

    void testComputeWCT();

    //Hier gaan we iedere combinatie van het algoritme proberen te testen
    void testRZInsertFirst();

    void createStats();
    void performVNDStats();
    void performSimpleStats();

};

#endif // TESTCLASS_H
