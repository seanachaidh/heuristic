#ifndef RZPERMUTATION_H
#define RZPERMUTATION_H

#include "initialsolution.h"
#include <list>

/**
 * @brief The RZPermutation class
 * This is an implementation of initial solution that can generate an initial solution according to
 * the RZ heuristic
 */
class RZPermutation : public InitialSolution
{

private:
    vector<int> rzPermutation();
    vector<vector<int>> rzGenerateNeighbourHood(vector<int> partial, int newJob);
    list<int> getInitialSolution();
    vector<pair<int, float> > getT();
    vector<int> findMinNeighbour(vector<vector<int>>);
public:
    RZPermutation(PfspInstance *instance);

    /**
     * @brief generateInitialSolution
     * @return An initial solution
     *
     * Generates an initial solution according to the RZ heuristic
     * This is a greedy heuristic and works by first sorting all jobs according to by their T value,
     * which is calculated by making the sum of all the processing times on each machine of a job and
     * divide it by its priority. The jobs are then sorted smallest T value first. We then start by taking the
     * first two T jobs of our sorted jobs and take that as our partial result. Then we take the next job and try to
     * insert that job in our paritial result so that our evaluation function is minimal. This continues until we have
     * inserted all the jobs
     *
     */
    virtual vector<int> generateInitialSolution();

    /**
     * @brief generateFromPartial
     * @return A complete solution
     *
     * Generates a solution from a partial solution plus some jobs that still
     * need to be inserted
     *
     */
    vector<int> generateFromPartial(vector<int> part, vector<int> toinsert);

    /**
     * @brief This function is used for sorting
     */
    static bool sortT(pair<int, float> x, pair<int, float> y);
    virtual string toString() {return "RZP";}

};

#endif // RZPERMUTATION_H
