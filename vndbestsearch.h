#ifndef VNDBESTSEARCH_H
#define VNDBESTSEARCH_H

#include "mainheader.h"

/**
 * @brief The VNDBestSearch class
 * @see VNDFirstSearch
 * This is the same as VNDFirstSearch but using the Best pivot rule while searching for a solution that is
 * at each iteration, searching for the best neighbour and if no improving neighbour can be found, move to the
 * next neighbourhood, until no neigbhourhood has an improving solution
 */
class VNDBestSearch: public PivotRule
{
private:
    vector<NeighbourHood*> hoods;
    vector<int> searchBest(vector<vector<int>> n, vector<int> current);

public:
    VNDBestSearch(PfspInstance* instance, InitialSolution* insol, bool changeup);
    virtual ~VNDBestSearch();

    virtual vector <int> searchForSolution(int timelimit);
};

#endif // VNDBESTSEARCH_H
