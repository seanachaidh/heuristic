Most of the details about the design is located in a separate reference manual (refman.pdf). The scientific report is located in report.pdf. In order to get started however, one needs the Qt libraries to be installed. The project is created using qmake, so in order to create the makefiles one needs to run qmake in the source directory. After that you can run the program as is or run the test set provided that a directory called instances with test instances is located in your current working directory.


- change directory to source directory
- make sure that Qt is installed
- run qmake
- run make or gmake

In reality, Qt is only used to run the test set. In future versions the possibility to exclude the test set from compilation will be provided. Run the program with parameter -h to get further help

