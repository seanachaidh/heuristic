#ifndef MAINHEADER_H
#define MAINHEADER_H
#include "htypes.h"
#include "pfspinstance.h"
#include "initialsolution.h"
#include "bestimprovement.h"
#include "firstimprovement.h"
#include "exchangeneighbourhood.h"
#include "insertneighbourhood.h"
#include "neighbourhood.h"
#include "pivotrule.h"
#include "randominitialsolution.h"
#include "rzpermutation.h"
#include "transposeneighbourhood.h"
#include "helpers.h"

#include "testclass.h"
#include "stochastictest.h"

#include "vndfirstsearch.h"
#include "vndbestsearch.h"
#include "antcolony.h"
#include "ant.h"

#include "iteratedgreedy.h"

#include <numeric>
#include <tuple>

void setupSystem();
void solveProblem(PivotRule* solver, PfspInstance *instance);
void printSolution(vector<int> sol, PfspInstance *instance);

#endif // MAINHEADER_H
