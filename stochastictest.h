#ifndef STOCHASTICTEST_H
#define STOCHASTICTEST_H

#include <QObject>
#include <QtTest/QtTest>
#include <QDir>
#include "mainheader.h"

class IteratedGreedy;
class AntColony;

class StochasticTest : public QObject
{
    Q_OBJECT
private:
    QStringList instancefiles;
    PfspInstance* loadFile(QString filename);

    QString name;

    PfspInstance* instance;

    IteratedGreedy* itergreedy;
    AntColony* aco;

public:
    StochasticTest();
private slots:
    void initTestCase();
    void testStochasticGreedy();
    void testAco();
    void performanceGreedy();
    void performanceAco();

};

#endif // STOCHASTICTEST_H
