#include "exchangeneighbourhood.h"

vector<int> ExchangeNeighbourHood::performSwap(vector<int> vec, int x, int y)
{
    vector<int> retval = vec;

    //perform swap
    int tmp = retval[x];
    retval[x] = retval[y];
    retval[y] = tmp;

    return retval;
}

ExchangeNeighbourHood::ExchangeNeighbourHood(PfspInstance* instance)
    :NeighbourHood(instance)
{
}

vector<vector<int> > ExchangeNeighbourHood::generateNeighbourHood(vector<int> state)
{
    vector<vector<int>> retval;

    for(unsigned int i = 0; i < state.size(); i++) {
        for(unsigned int j = i; j < state.size(); j++) {
            retval.push_back(this->performSwap(state,i,j));
        }
    }

    return retval;
}
