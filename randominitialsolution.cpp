#include "randominitialsolution.h"

RandomInitialSolution::RandomInitialSolution(PfspInstance* instance)
    :InitialSolution(instance)
{

}

vector<int> RandomInitialSolution::generateInitialSolution()
{
    vector <int> sol;
    for (int i = 0; i < this->instance->getNbJob();i++)
        sol.push_back(i);

    random_shuffle(sol.begin(),sol.end());

    return sol;
}
