#ifndef TRANSPOSENEIGHBOURHOOD_H
#define TRANSPOSENEIGHBOURHOOD_H

#include "neighbourhood.h"

/**
 * @brief The TransposeNeighbourHood class
 * This neigbourhood
 */
/**
 * @brief The TransposeNeighbourHood class
 *
 * This class represents the transpose neighbourhood created by exchanging a given job with the job next to it
 * please note that this is a subset of the insert neighbourhood
 *
 */
class TransposeNeighbourHood : public NeighbourHood
{
private:
    vector<int> performSwap(vector<int> vec, int i);
public:
    TransposeNeighbourHood(PfspInstance* instance);
    virtual string toString() {return "Tr";}
    virtual vector<vector<int>> generateNeighbourHood(vector<int> state);
};

#endif // TRANSPOSENEIGHBOURHOOD_H
