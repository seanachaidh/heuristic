#include "testclass.h"


TestClass::TestClass(int limit, bool vnd, bool changeup)
    :QObject()
{
    this->limit = limit;
    this->vnd = vnd;
    this->changeup = changeup;
}

PfspInstance *TestClass::loadFile(QString filename)
{
    PfspInstance* instance = new PfspInstance();

    string realpath = "instances/";
    realpath.append(filename.toStdString());
    bool foundfile = instance->readDataFromFile(realpath.c_str());

    if(!foundfile) {
        cerr << "Instance file not found: " << realpath << endl;
        exit(1);
    }

    return instance;
}



PivotRule *TestClass::createTestInstance(Pivot pivot, Neighbour neighbour, Solution solution, QString filename)
{

    PfspInstance* instance = loadFile(filename);

    NeighbourHood* nhood;
    InitialSolution* insol;
    PivotRule* rule;

    switch (solution) {
    case RANDOM:
        insol = new RandomInitialSolution(instance);
        break;
    case SIMPLIFIED:
        insol = new RZPermutation(instance);
        break;
    }
    switch(neighbour){
    case TRANSPOSE:
        nhood = new TransposeNeighbourHood(instance);
        break;
    case INSERT:
        nhood = new InsertNeighbourHood(instance);
        break;
    case EXCHANGE:
        nhood = new ExchangeNeighbourHood(instance);
        break;
    default:
        nhood = NULL;

    }

    switch (pivot) {
    case BEST:
        if (vnd)
            rule = new VNDBestSearch(instance, insol,changeup);
        else
            rule = new BestImprovement(instance,nhood,insol);
        break;
    case FIRST:
        if (vnd)
            rule = new VNDFirstSearch(instance, insol,changeup);
        else
            rule = new FirstImprovement(instance,nhood,insol);
        break;
    }

    return rule;
}

PivotRule *TestClass::createTestInstance(Pivot pivot, Solution solution, QString filename)
{
    PfspInstance* instance = loadFile(filename);

    InitialSolution* insol;
    PivotRule* rule;

    switch (solution) {
    case RANDOM:
        insol = new RandomInitialSolution(instance);
        break;
    case SIMPLIFIED:
        insol = new RZPermutation(instance);
        break;
    }

    switch (pivot) {
    case BEST:
        rule = new VNDBestSearch(instance, insol,changeup);
        break;
    case FIRST:
        rule = new VNDFirstSearch(instance, insol,changeup);
        break;
    }

    return rule;
}

void TestClass::initTestCase()
{
    //prority matrix creation
    vector<int> prior = {1,2,4,2,3};
    vector<vector<long int>> matrix;
/*
    matrix.push_back({3,3,4,2,3});
    matrix.push_back({2,1,3,3,1});
    matrix.push_back({4,2,1,2,3});
*/
    matrix.push_back({3,2,4});
    matrix.push_back({3,1,2});
    matrix.push_back({4,3,1});
    matrix.push_back({2,3,2});
    matrix.push_back({3,1,3});

    this->testInstance = new PfspInstance(matrix,prior);

    this->secondInstance = new PfspInstance();
    bool filecheck = this->secondInstance->readDataFromFile("instances/50_20_01");
    if (!filecheck) {
        QFAIL("file could not be opened");
    }

    QDir instancedir("instances");
    instancefiles = instancedir.entryList(QDir::Files);
}

void TestClass::testInsertNeighbourHood()
{
    InsertNeighbourHood* n = new InsertNeighbourHood(testInstance);
    vector<vector<int>> output = n->generateNeighbourHood({1,2,3,4,5});

    //Dit slaagt in principe altijd en is dus eigenlijk geen echte test, maar ik zou niet weten hoe ik iedere waarde kan nakijken
    printMatrix(output);

}

void TestClass::testRZHeuristic()
{
    InitialSolution* init = new RZPermutation(this->testInstance);
    vector<int> sol = init->generateInitialSolution();
    cout << "Test of rzheuristic results"  << endl;
    for(int i: sol){
        cout << i << ",";
    }
    cout << endl;
    cout << "WTC: " << testInstance->newComputeWCT(sol) << endl;
}

void TestClass::testComputeWCT()
{
    vector<int> testData1 = {2,4};
    int test1 = testInstance->newComputeWCT(testData1);
    QCOMPARE(test1,65);
}

void TestClass::testRZInsertFirst()
{
    InitialSolution* initsol = new RZPermutation(secondInstance);
    NeighbourHood* neigh = new InsertNeighbourHood(secondInstance);
    PivotRule* solver = new FirstImprovement(secondInstance, neigh,initsol);
    vector<int> result = solver->searchForSolution();

    printSolution(result, secondInstance);
}

void TestClass::createStats()
{
    if(this->vnd) {
        performVNDStats();
    } else {
        performSimpleStats();
    }
}

void TestClass::performVNDStats()
{

    ofstream testresults;
    testresults.open("stats_vnd.txt");
    testresults << "instname;pivot;initial;result;time" << endl;


    for(QString file: instancefiles) {
        cout << "Testing " << file.toStdString() << endl;
        for (Solution s: solutions) {
            for(Pivot p: pivots) {

                cout << file.toStdString() << ";" << pivotToString(p) << ";" << solutionToString(s) << ";" << "tmp_wct" << endl;

                PivotRule*  tmp_pivot = (createTestInstance(p,s,file));
                QElapsedTimer timer;
                timer.start();
                vector<int> tmp_solution = tmp_pivot->searchForSolution(limit);
                quint64 delta_t = timer.elapsed();

                long tmp_wct = tmp_pivot->getInstance()->newComputeWCT(tmp_solution);
                testresults << file.toStdString() << ";" << pivotToString(p) <<  ";" << solutionToString(s) << ";"
                            << tmp_wct << ";" << delta_t << endl;
                delete tmp_pivot;
            }
        }
    }

    testresults.close();
}

void TestClass::performSimpleStats()
{

    ofstream testresults;
    testresults.open("stats.txt");
    testresults << "instname;pivot;neighbour;initial;result;time" << endl;


    for(QString file: instancefiles) {
        cout << "Testing " << file.toStdString() << endl;
        for(Neighbour n: neighbours) {
            for (Solution s: solutions) {
                for(Pivot p: pivots) {

                    cout << file.toStdString() << ";" << pivotToString(p) << ";"
                           << neighbourToString(n) << ";" << solutionToString(s) << ";" << "tmp_wct" << endl;

                    PivotRule*  tmp_pivot = (createTestInstance(p,n,s,file));
                    QElapsedTimer timer;
                    timer.start();
                    vector<int> tmp_solution = tmp_pivot->searchForSolution(limit);
                    quint64 delta_t = timer.elapsed();

                    long tmp_wct = tmp_pivot->getInstance()->newComputeWCT(tmp_solution);
                    testresults << file.toStdString() << ";" << pivotToString(p) << ";"
                           << neighbourToString(n) << ";" << solutionToString(s) << ";"
                           << tmp_wct << ";" << delta_t << endl;
                    delete tmp_pivot;
                }
            }
        }
    }

    testresults.close();
}
