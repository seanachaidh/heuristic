#!/bin/sh

mkdir output_aco
touch stochastic_aco.txt

echo "id;instance;result" >> stochastic_aco.txt

for inst in `ls instances`; do
	input=instances/${inst}
	echo "Input " ${input}
	for i in `seq 1 5`; do
		echo "Testing"
		echo "Instance: " $inst
		echo "iteration: " $i
		outfile=output_aco/${inst}_$i
		echo "Output: " $outfile
		./heuristic --instance ${input} --aco --limit 104500 > ${outfile}
		result=`cat ${outfile} | grep "WCT" | cut -d ":" -f 2`
		echo "${i};${inst};${result}" >> stochastic_aco.txt
	done
done
