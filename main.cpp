#include <cstring>
#include <string>
#include <ctime>

#include <iostream>
#include <unistd.h>

#include <QTest>

#include "mainheader.h"

Solution sol;
Pivot piv;
Neighbour neigh;

PivotRule* rule;

string filename;
PfspInstance* instance;

int limit = -1;
bool vnd = false;
bool changeup = false;

bool iterated_greedy = false;
bool aco = false;

int sigma = 3;
int maxiterations = 5;

int numants = 5;
double rho = 0.2;


using namespace std;

void setupSystem() {
    instance = new PfspInstance();
    bool foundfile = instance->readDataFromFile(filename.c_str());

    if(!foundfile) {
        cerr << "Instance file not found" << endl;
        exit(1);
    }


    NeighbourHood* nhood;
    InitialSolution* insol;

    switch (sol) {
    case RANDOM:
        insol = new RandomInitialSolution(instance);
        break;
    case SIMPLIFIED:
        insol = new RZPermutation(instance);
        break;
    }

    switch(neigh){
    case TRANSPOSE:
        nhood = new TransposeNeighbourHood(instance);
        break;
    case INSERT:
        nhood = new InsertNeighbourHood(instance);
        break;
    case EXCHANGE:
        nhood = new ExchangeNeighbourHood(instance);
        break;
    }

    switch (piv) {
    case BEST:
        if (vnd)
            rule = new VNDBestSearch(instance, insol,changeup);
        else
            rule = new BestImprovement(instance,nhood,insol);
        break;
    case FIRST:
        if (vnd)
            rule = new VNDFirstSearch(instance, insol,changeup);
        else
            rule = new FirstImprovement(instance,nhood,insol);
        break;
    }

    if(iterated_greedy) {
        rule = new IteratedGreedy(instance, sigma, maxiterations);
    }

    if(aco) {
        rule = new AntColony(instance, numants,maxiterations,rho);
    }

}

void printSolution(vector<int> sol, PfspInstance* instance){
    cout << "Found solution:" << endl;
    unsigned int ss = sol.size();
    for(unsigned int i = 0; i < ss; i++)
        (i < (ss - 1))? cout << sol[i] << "," : cout << sol[i] << endl;

    cout << "WCT:" << instance->newComputeWCT(sol) << endl;
    cout << "Length of solution:" << sol.size() << endl;
}

void solveProblem(PivotRule* solver, PfspInstance* instance) {
    vector <int> solution = solver->searchForSolution(limit);
    printSolution(solution, instance);
}

void showHelp() {

    vector<pair<string, string>> optvector = {
        {"--first", "use the first pivot rule"},
        {"--best", "use the best pivot rule"},
        {"--limit l", "set execution time limit to l in minutes"},
        {"--changeup", "Use the switched up neighbourhood <TRANS,EXCH,INS> used in VND"},
        {"--vnd", "Use variable neighbourhood descent"},
        {"-t", "Run the testset instead of the real program (might take a while and instance folder must be in current program location"},
        {"--transpose", "Use the transpose neighbourhood for non vnd"},
        {"--exchange", "Use the exchange neighbourhood for non vnd"},
        {"--insert", "Use the insert neighbourhood for non vnd"},
        {"--srz", "Use an initial solution according to the rz heuristic"},
        {"--random", "Use a random initial solution"},

    };

    cout << "Heuristic flow shop solver"  << endl;
    cout << "Usage ./heuristic --instance <instance> [options]" << endl;
    cout << "One can provide the following options considering at least one pivot rule, one neighbourhood and one initial solution is chosen" << endl;
    cout << "Neighbourhood options are ignored while in VND mode and changup is ignored in non-vnd mode" << endl;
    for(pair<string,string> opt: optvector){
        cout << opt.first << ":" << endl << "\t" << opt.second << endl;
    }
}

PfspInstance* loadOptimInstance() {
    PfspInstance* retval = new PfspInstance();
    bool foundfile = retval->readDataFromFile("instances/50_20_01");
    if(!foundfile)
        retval = NULL;
    return retval;
}

void optimaliseIterative(){
    vector<int> sigmas = {1,2,3};
    vector<int> iterations = {5,10,15};

    PfspInstance* inst = loadOptimInstance();

    ofstream outfile;
    outfile.open("optimiter.txt");

    outfile << "sig;iters;mures" << endl;

    vector<IterOptimResults> results;
    for(int sig: sigmas){
        for(int iters: iterations) {
            cout << "Optimalising: " << endl;
            cout << "Sigma: " << sig << endl;
            cout << "Amount of iterations: " << iters << endl;

            //We try each variant 5 times
            vector<long> tmpres;
            for(int i = 0; i < 5; i++){
                IteratedGreedy* greed = new IteratedGreedy(inst,sig,iters);
                vector<int> solu = greed->searchForSolution();
                tmpres.push_back(inst->newComputeWCT(solu));
            }

            double mean = accumulate(tmpres.begin(),tmpres.end(),0);
            IterOptimResults res;

            res.iterations = iters;
            res.meanresult = mean/5;
            res.sigma = sig;

            results.push_back(res);
        }
    }

    for(IterOptimResults r: results) {
        outfile << r.sigma  <<";" << r.iterations << ";" << r.meanresult << endl;
    }

    outfile.close();

}

void optimaliseAco() {
    vector<double> rhos = {0.2, 0.5, 0.8};
    vector<int> antnumbers {20,50,80};
    vector<int> iterations = {5,10,15};

    PfspInstance* instance = loadOptimInstance();

    ofstream outfile;
    outfile.open("optimaco.txt");

    outfile << "rho;ants;iterations;mures" << endl;

    vector<AcoOptimResults> lines;

    for(double r: rhos) {
        for(int a: antnumbers) {
            for(int i: iterations){
                cout << "Optimalising ACO" << endl;
                cout << "Rho: " << r << endl;
                cout << "Ants: " << a << endl;
                cout << "Iterations: " << i << endl;


                vector<long> results;
                for(int j = 0; j < 5; j++){
                    AntColony* antcolony = new AntColony(instance,a,i,r);
                    vector<int> solu = antcolony->searchForSolution(30*60);
                    long res = instance->newComputeWCT(solu);
                    results.push_back(res);
                }

                double mean = accumulate(results.begin(),results.end(),0)/5;

                AcoOptimResults line;
                line.iterations = i;
                line.numants = a;
                line.rho = r;
                line.meanresult = mean;

                lines.push_back(line);
            }
        }
    }

    //Put all the results in the file
    for(AcoOptimResults optim: lines){
        outfile << optim.rho << ";" << optim.numants <<
                   ";" << optim.iterations << ";" << optim.meanresult << endl;
    }
    outfile.close();
}

void extractArguments(char **argv, int argc)
{
    for(int i = 0; i < argc; i++) {
        if(strcmp(argv[i],"--first") == 0){
            piv = FIRST;
        } else if (strcmp(argv[i], "--seed") == 0) {
            srand(atoi(argv[i+1]));
            i++;
        } else if (strcmp(argv[i], "-h") == 0){
            showHelp();
            exit(0);
        } else if (strcmp(argv[i], "--limit") == 0) {
            limit = atoi(argv[i+1]);
            i++;
        } else if (strcmp(argv[i], "--changeup") == 0) {
            changeup = true;
        } else if (strcmp(argv[i], "--vnd") == 0) {
            vnd = true;
        } else if(strcmp(argv[i], "-t") == 0) {
            cout << "running testset" << endl;
            if(strcmp(argv[i+1], "stochastic") == 0) {
                StochasticTest test;
                QTest::qExec(&test);
                exit(0);
            } else if (strcmp(argv[i+1], "iterative") == 0) {
                TestClass test(limit,vnd,changeup);
                QTest::qExec(&test);
                exit(0);
            } else {
                cout << "Please choose a test to run!" << endl;
                exit(1);
            }

        } else if(strcmp(argv[i], "--best") == 0) {
            piv = BEST;
        } else if (strcmp(argv[i], "--transpose") == 0) {
            neigh = TRANSPOSE;
        } else if (strcmp(argv[i], "--exchange") == 0) {
            neigh = EXCHANGE;
        } else if (strcmp(argv[i], "--insert") == 0){
            neigh = INSERT;
        } else if (strcmp(argv[i], "--srz") == 0) {
            sol = SIMPLIFIED;
        } else if (strcmp(argv[i], "--random") == 0) {
            sol = RANDOM;
        } else if (strcmp(argv[i], "--instance") == 0){
            filename = argv[i+1];
            i++;
        } else if(strcmp(argv[i], "--iteratedgreedy") == 0){
            iterated_greedy = true;
        } else if(strcmp(argv[i], "--aco") == 0){
            aco = true;
        } else if (strcmp(argv[i], "--numants") == 0) {
            numants = atoi(argv[i+1]);
            i++;
        } else if(strcmp(argv[i], "--rho") == 0){
            rho = atof(argv[i+1]);
            i++;
        } else if (strcmp(argv[i], "--optimalise") == 0){
            cout << "Entering optimalisation: " << endl;
            if(strcmp(argv[i+1], "aco") == 0){
                optimaliseAco();
                exit(0);
            } else if (strcmp(argv[i+1], "iterative") == 0) {
                optimaliseIterative();
                exit(0);
            } else {
                cout << "Please choose an algorithm to optimalise" << endl;
                exit(1);
            }
        }
    }
}

int main(int argc, char *argv[])
{
    srand(time(NULL));
    extractArguments(argv, argc);

    if(filename.empty()) {
        cout << "An instance file must be provided" << endl;
        exit(1);
    }

    setupSystem();
    solveProblem(rule, instance);

    return 0;
}
