TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG += qt
QT += testlib

# With C++11 support
greaterThan(QT_MAJOR_VERSION, 4){
CONFIG += c++11
} else {
QMAKE_CXXFLAGS += -std=c++0x
}


SOURCES += main.cpp \
    pfspinstance.cpp \
    helpers.cpp \
    testclass.cpp \
    initialsolution.cpp \
    randominitialsolution.cpp \
    rzpermutation.cpp \
    neighbourhood.cpp \
    transposeneighbourhood.cpp \
    exchangeneighbourhood.cpp \
    insertneighbourhood.cpp \
    pivotrule.cpp \
    bestimprovement.cpp \
    firstimprovement.cpp \
    vndfirstsearch.cpp \
    vndbestsearch.cpp \
    ant.cpp \
    iteratedgreedy.cpp \
    antcolony.cpp \
    stochastictest.cpp \
    runtimetest.cpp

HEADERS += \
    pfspinstance.h \
    htypes.h \
    helpers.h \
    testclass.h \
    initialsolution.h \
    randominitialsolution.h \
    rzpermutation.h \
    neighbourhood.h \
    transposeneighbourhood.h \
    exchangeneighbourhood.h \
    insertneighbourhood.h \
    pivotrule.h \
    bestimprovement.h \
    firstimprovement.h \
    mainheader.h \
    vndfirstsearch.h \
    vndbestsearch.h \
    ant.h \
    iteratedgreedy.h \
    antcolony.h \
    stochastictest.h \
    runtimetest.h

OTHER_FILES += \
    createStats.sh

DISTFILES += \
    stoch_aco.sh \
    runtime_greedy.sh \
    runtime_aco.sh
