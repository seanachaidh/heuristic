#include "stochastictest.h"

PfspInstance *StochasticTest::loadFile(QString filename)
{
    return NULL;
}

StochasticTest::StochasticTest()
    :QObject()
{
    name = "Hello";
}

void StochasticTest::initTestCase()
{
    this->instance = new PfspInstance();
    bool filecheck = this->instance->readDataFromFile("instances/50_20_30");

    if(!filecheck)
        QFAIL("File could not be opened");
    QDir instancedir("instances");
    itergreedy = new IteratedGreedy(instance,3,5);
    aco = new AntColony(instance,100,5,0.3); //Maybe look here in the book of Dorigo
    instancefiles = instancedir.entryList(QDir::Files);
}

void StochasticTest::testStochasticGreedy()
{
    cout << "Test of itterated greedy"  << endl << "---------------" << endl;
    vector<int> sol = itergreedy->searchForSolution(10);
    for(int i: sol){
        cout << i << ",";
    }
    cout << endl;

    cout << "WCT: "<< instance->newComputeWCT(sol) << endl;
    cout << "Solution length: " << sol.size() << endl;

}

void StochasticTest::testAco()
{
    cout << "Test of ACO"  << endl << "---------------" << endl;
    vector<int> sol = aco->searchForSolution(10);
    for(int i: sol){
        cout << i << ",";
    }
    cout << endl;

    cout << "WCT: "<< instance->newComputeWCT(sol) << endl;
    cout << "Solution length: " << sol.size() << endl;
}

void StochasticTest::performanceGreedy()
{
    int testtime = 104500;
    ofstream outfile;
    outfile.open("stochastic_greedy.txt");
    outfile << "id;instance;result" << endl;
    for(QString file: instancefiles){
        PfspInstance* inst = new PfspInstance();
        QString finalfile = "instances/" + file;
        inst->readDataFromFile(finalfile.toStdString().c_str());
        for(int i = 1; i <= 5; i++){
            IteratedGreedy* solver = new IteratedGreedy(inst,3,15);
            vector<int> sol = solver->searchForSolution(testtime);
            long result = inst->newComputeWCT(sol);

            outfile << i << ";" << file.toStdString() << ";" << result << endl;

        }
    }
    outfile.close();
}

void StochasticTest::performanceAco()
{
    int testtime = 104500;
    ofstream outfile;
    outfile.open("stochastic_aco.txt");
    outfile << "id;instance;result" << endl;
    for(QString file: instancefiles){
        PfspInstance* inst = new PfspInstance();
        QString finalfile = "instances/" + file;
        inst->readDataFromFile(finalfile.toStdString().c_str());
        for(int i = 1; i <= 5; i++){
            //IteratedGreedy* solver = new AntColony(inst
            //vector<int> sol = solver->searchForSolution(testtime);
            //long result = inst->newComputeWCT(sol);

            //outfile << i << ";" << file.toStdString() << ";" << result << endl;

        }
    }
    outfile.close();
}
