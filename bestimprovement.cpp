#include "bestimprovement.h"

vector<int> BestImprovement::searchBest(vector<vector<int> > n, vector<int> current)
{
    vector<int> retval;
    long currentWCT = this->getInstance()->newComputeWCT(current);
    for(vector<int> v: n){
        long newWCT = this->getInstance()->newComputeWCT(v);
        if(newWCT < currentWCT){
            currentWCT = newWCT;
            retval = v;
        }
    }

    return retval;
}

BestImprovement::BestImprovement(PfspInstance* instance, NeighbourHood* hood, InitialSolution* initsol)
    :PivotRule(instance,hood,initsol)
{

}

BestImprovement::BestImprovement(PfspInstance *instance, NeighbourHood *hood, vector<int> startpoint)
    :PivotRule(instance,hood,NULL)
{
    this->startpoint = startpoint;
}

vector<int> BestImprovement::searchForSolution(int timelimit = -1)
{
    long timenow = time(NULL);
    vector<int> pi;
    if(this->getInitialSolution() == NULL){
        pi = this->startpoint;
    } else {
        pi = this->getInitialSolution()->generateInitialSolution();
    }
    for(;;) {
        if(timelimit > 0) {
            long delta_time = time(NULL) - timenow;
            if(ceil(delta_time/60) > timelimit)
                break;
        }
        vector<vector<int>> N_pi = this->getNeighbourHood()->generateNeighbourHood(pi);
        vector<int> tocheck = searchBest(N_pi,pi);
        if(tocheck.empty())
            break;
        else
            pi = tocheck;
    }
    return pi;
}
