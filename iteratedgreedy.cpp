#include "iteratedgreedy.h"

//probreer hier zoveel keer als sigma toelaat te perturberen. Dat is, twee jobs van plaats veranderen.
//Let op. Een verplaatsing kan twee keer gebeuren
pair<vector<int>,vector<int>> IteratedGreedy::destruct(vector<int> sol)
{
    vector<int> destructed;
    for(int i = 0; i < sigma; i++){
        int randint = rand() % sol.size();
        destructed.push_back(sol[randint]);
        sol.erase(sol.begin()+randint);
    }

    return make_pair(sol,destructed);
}

//TODO: Dit is een methode die eigenlijk gedeeld wordt met het ACO algoritme
bool IteratedGreedy::shouldTerminate()
{
    quint64 elapsed = timer.elapsed();
    if((elapsed) > timelimit)
        return true;
    else
        return false;

}

//What to do with the timelimit here?
vector<int> IteratedGreedy::searchForSolution(int timelimit)
{
    this->timelimit = timelimit;
    timer.start();
    while(!shouldTerminate()){

        //cout << "Iteration: " << endl << "Iteration: " << iterations << endl << "Max: " << maxiterations

        vector<int> copy = best_solution;
        pair<vector<int>,vector<int>> d = destruct(copy);
        //This isn't very clean
        copy = ((RZPermutation*)this->getInitialSolution())->generateFromPartial(d.first,d.second);

        //TODO: Opgepast mogelijks geheugenlek
        BestImprovement* tmpsearch = new BestImprovement(this->getInstance(),this->getNeighbourHood(),copy);
        copy = tmpsearch->searchForSolution(-1);

        long newgoal = this->getInstance()->newComputeWCT(copy);

        if(newgoal < goal_evaluation){
            goal_evaluation = newgoal;
            best_solution = copy;
        }
        this->iterations = this->iterations + 1;
    }

    return best_solution;

}

//TODO misschien hier de mogelijkheid implementeren tot het veranderen van neighbourhood

IteratedGreedy::IteratedGreedy(PfspInstance *instance, int sigma, int maxiterations)
    :PivotRule(instance, new InsertNeighbourHood(instance),new RZPermutation(instance))
{
    this->sigma = sigma;
    this->maxiterations = maxiterations;
    this->iterations = 0;

    //Perform constructive heuristic. Neemt waarschijnlijk nogal veel tijd in beslag
    BestImprovement* searcher = new BestImprovement(this->getInstance(), this->getNeighbourHood(),this->getInitialSolution());
    this->best_solution = searcher->searchForSolution(-1);
    this->goal_evaluation = this->getInstance()->newComputeWCT(this->best_solution);

}
