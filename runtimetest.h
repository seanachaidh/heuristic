#ifndef RUNTIMETEST_H
#define RUNTIMETEST_H

#include <QObject>
#include <QTest>
#include <QElapsedTimer>
#include <QStringList>

class RunTimeTest : public QObject
{
    Q_OBJECT
public:
    explicit RunTimeTest(QObject *parent = 0);

    QStringList files;

private slots:
    void runTimeAco();
    void runTimeGreedy();
    void initTestCase();
};
#endif // RUNTIMETEST_H
