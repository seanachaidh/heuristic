#ifndef INSERTNEIGHBOURHOOD_H
#define INSERTNEIGHBOURHOOD_H

#include "neighbourhood.h"
/**
 * @brief The InsertNeighbourHood class
 *
 * This type of neighbourhood works by insertion. That is it defines a neighbour as
 * a list where one of the jobs has been taken out and inserted somewhere else in the solution
 *
 */
class InsertNeighbourHood : public NeighbourHood
{
private:
    vector<int> performInsert(vector<int> vec, int x, int y);
public:
    InsertNeighbourHood(PfspInstance* instance);
    virtual string toString() {return "In";}
    virtual vector<vector<int>> generateNeighbourHood(vector<int> state);
};

#endif // INSERTNEIGHBOURHOOD_H
