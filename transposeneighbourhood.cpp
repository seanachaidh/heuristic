#include "transposeneighbourhood.h"

vector<int> TransposeNeighbourHood::performSwap(vector<int> vec, int i)
{
    vector<int> retval = vec; //Oppervlakkige kopie
    int l = vec.size();
    if(i == (l - 1)) {
        int tmp = retval[i];
        retval[i] = retval[0];
        retval[0] = tmp;
    } else {
        int tmp = retval[i];
        retval[i] = retval[i+1];
        retval[i+1] = tmp;
    }

    return retval;
}

TransposeNeighbourHood::TransposeNeighbourHood(PfspInstance *instance)
    :NeighbourHood(instance)
{

}

vector<vector<int> > TransposeNeighbourHood::generateNeighbourHood(vector<int> state)
{
    vector<vector<int>> retval;

    for(unsigned int i = 0; i < state.size(); i++) {
        //Ik denk dat de volgorde waarin ik de buren plaats niet al te veel uitmaakt.
        retval.push_back(this->performSwap(state, i));
    }

    return retval;
}
