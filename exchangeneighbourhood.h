#ifndef EXCHANGENEIGHBOUR_H
#define EXCHANGENEIGHBOUR_H

#include "neighbourhood.h"

/**
 * @brief The ExchangeNeighbourHood class
 *
 * This class implements a neighbourhood in which a neighbour is defined by an
 * exchange in position of two jobs of a given solution
 *
 */
class ExchangeNeighbourHood : public NeighbourHood
{
private:
    vector<int> performSwap(vector<int> vec, int x, int y);
public:
    ExchangeNeighbourHood(PfspInstance* instance);
    virtual string toString() {return "Ex";}
    virtual vector<vector<int>> generateNeighbourHood(vector<int> state);
};

#endif // EXCHANGENEIGHBOUR_H
