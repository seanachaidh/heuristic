#include "pivotrule.h"

PivotRule::PivotRule(PfspInstance *instance, NeighbourHood *hood, InitialSolution *initsol)
{
    this->hood = hood;
    this->initsol = initsol;
    this->instance = instance;
}

PivotRule::~PivotRule()
{
    delete hood;
    delete initsol;
    //delete instance;
}
