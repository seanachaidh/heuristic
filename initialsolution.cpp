#include "initialsolution.h"

InitialSolution::InitialSolution(PfspInstance* instance)
{
    this->instance = instance;
}

InitialSolution::~InitialSolution()
{
    delete instance;
}
