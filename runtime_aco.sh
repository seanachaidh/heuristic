#!/bin/sh

touch runtime_aco.txt
mkdir runtime_aco_output
echo "instance;time;result" >> runtime_aco.txt

for inst in `ls smallinst`; do
	input=smallinst/${inst}
	echo "Input " ${input}
	for time in `seq 1 41800 1045000`; do
		echo "Testing"
		echo "Instance: " ${inst}
		echo "Time: " ${time}
		outfile=runtime_aco_output/${inst}_${time}
		./heuristic --instance ${input} --aco --limit ${time} > ${outfile}
		result=`cat ${outfile} | grep "WCT" | cut -d ":" -f 2`
		echo "${inst};${time};${result}" >> runtime_aco.txt
	done
done
