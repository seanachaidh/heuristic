#ifndef HELPERS_H
#define HELPERS_H
//Standard c libraries
#include <cstdlib>
#include <string>
#include "htypes.h"

// C++ libraries
#include <iostream>
#include <vector>
using namespace std;

int generateRndPosition(int min, int max);
void printMatrix(vector<vector<int>> mat);

// tostrngs
string pivotToString(Pivot p);
string neighbourToString(Neighbour n);
string solutionToString(Solution s);

#endif // HELPERS_H
