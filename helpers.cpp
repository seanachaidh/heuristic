#include "helpers.h"

int generateRndPosition(int min, int max)
{
  return ( rand() % max + min );
}


string solutionToString(Solution sol) {
    string retval;
    switch(sol) {
    case RANDOM:
        retval = "RND";
        break;
    case SIMPLIFIED:
        retval = "RZP";
        break;
    }

    return retval;
}

string neighbourToString(Neighbour n) {
    string retval;
    switch(n) {
    case EXCHANGE:
        retval = "EXCH";
        break;
    case TRANSPOSE:
        retval = "TRANS";
        break;
    case INSERT:
        retval = "INS";
        break;
    }

    return retval;
}

string pivotToString (Pivot p) {
    string retval;
    switch(p) {
    case BEST:
        retval = "BEST";
        break;
    case FIRST:
        retval = "FIRST";
        break;
    }

    return retval;
}


void printMatrix(vector<vector<int> > mat)
{
    cout << "matrix printing: " << endl;
    for(unsigned int i = 0; i < mat.size(); i++) {
        vector<int> tmp = mat[i];
        for(unsigned int j = 0; j < tmp.size(); j++) {
            cout << tmp[j] << ",";
        }
        cout << endl;
    }
}
