#ifndef VNDFIRSTSEARCH_H
#define VNDFIRSTSEARCH_H

#include "mainheader.h"

/**
 * @brief The VNDFirstSearch class
 *
 * This class implements Variable Neighbourhood descent using first improvement
 * This means that there will be no explicitly defined neighbourhood. Instead, if
 * we are at a local optimum, we will switch from one neighbourhood to another, until
 * no improving solution can be found in any neighbourhood
 *
 */
class VNDFirstSearch : public PivotRule
{
private:
    vector<NeighbourHood*> hoods;

    vector<int> findFirstImprovement(vector<vector<int>> neighbours,vector<int> current);
    vector<vector<int>> generatePartialNeighbourhood(vector<int> current, int fromwhere, int hood);

public:
    VNDFirstSearch(PfspInstance* instance, InitialSolution* initsol, bool changeup);
    virtual ~VNDFirstSearch();

    virtual vector<int> searchForSolution(int timelimit = -1);
    virtual string toString() {return "VND";}

};

#endif // VNDSEARCH_H
