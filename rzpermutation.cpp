#include "rzpermutation.h"

RZPermutation::RZPermutation(PfspInstance* instance)
    :InitialSolution(instance)
{

}

vector<int> RZPermutation::generateInitialSolution()
{
    long nbJob = this->instance->getNbJob();
    //Veranderd naar lijsten om het makkelijker en sneller te maken
    list<int> T = this->getInitialSolution();
    vector<int> result; //Dit is nog steeds een vector. Misschien moet ik dit later naar een lijst veranderen

    //We beginnen met een initiële oplossing
    result.push_back(T.front());T.pop_front();
    result.push_back(T.front());T.pop_front();

    for(int i = 2; i < nbJob; i++) {
        vector<vector<int>> neighbours = rzGenerateNeighbourHood(result,T.front());T.pop_front();
        result = findMinNeighbour(neighbours);
    }
    return result;
}

vector<int> RZPermutation::generateFromPartial(vector<int> part, vector<int> toinsert)
{
    vector<int> result = part;
    for(int i = 0; i < toinsert.size(); i++){
        vector<vector<int>> neighbours = rzGenerateNeighbourHood(result,toinsert[i]);
        result = findMinNeighbour(neighbours);
    }

    return result;
}

vector<pair<int,float>> RZPermutation::getT()
{
    long nbJob = this->instance->getNbJob();
    long nbMac = this->instance->getNbMac();
    vector <pair<int,float>> T (nbJob);

    //Berekenen van de initiële T vector
    //Spijtig van de dubbele forloop
    for(int i = 0; i < nbJob; i++) {
        float curweight = this->instance->getPriority(i);
        float cursum = 0;
        for(int j = 0; j < nbMac; j++) {
            cursum += this->instance->getTime(i,j);
            //cursum += processingTimesMatrix[i][j];
        }
        //TODO Hier treedt een afrondfout op.
        T[i] = make_pair(i,cursum/curweight);
    }
    return T;
}

//Hier wordt er eentje vergeten
vector<vector<int> > RZPermutation::rzGenerateNeighbourHood(vector<int> partial, int newJob)
{
    vector<vector<int>> result;
    for(unsigned long i = 0; i < partial.size() + 1; i++) {
        vector<int> tmp(partial);
        tmp.insert(tmp.begin()+i,newJob); //TODO: Hier kan potentiëel een fout zitten.
        result.push_back(tmp);
    }
    return result;
}

list<int> RZPermutation::getInitialSolution()
{
    long nbJob = this->instance->getNbJob();
    list<int> result;
    vector<pair<int,float>> T = this->getT();
    sort(T.begin(),T.end(),RZPermutation::sortT);

    for(int i = 0;i < nbJob; i++) {
        result.push_back(T[i].first);
    }

    return result;
}

vector<int> RZPermutation::findMinNeighbour(vector<vector<int>> neigh)
{
    vector<int>current = neigh[0];
    float current_wct = this->instance->newComputeWCT(current);
    //Skip the first
    for(unsigned long i = 1; i < neigh.size(); i++){
        vector<int> new_current = neigh[i];
        float new_wct = this->instance->newComputeWCT(new_current);
        if(new_wct < current_wct) {
            current = new_current;
            current_wct = new_wct;
        }
    }
    return current;
}

bool RZPermutation::sortT(pair<int,float> i, pair<int,float> j)
{
    float x = i.second;
    float y = j.second;
    return x < y; //TODO: Eens nakijken of ik de bekjes omgekeerd moet zetten
}

