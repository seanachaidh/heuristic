#ifndef ITERATEDGREEDY_H
#define ITERATEDGREEDY_H

#include "mainheader.h"

#include <QElapsedTimer>

class IteratedGreedy: public PivotRule
{
private:
    int maxiterations;
    int iterations;

    vector<int> best_solution;
    long goal_evaluation;

    quint64 timelimit;
    QElapsedTimer timer;

    bool shouldTerminate();

    /*
     * Perturbation intensity
     * How many times should we perturbate?
     */
    int sigma;

    /*
     * Actual perturbation
     */
    pair<vector<int>,vector<int>> destruct(vector<int> sol);

public:
    IteratedGreedy(PfspInstance* instance,int sigma,int maxiterations);
    virtual vector<int> searchForSolution(int timelimit = -1);
};

#endif // ITERATEDGREEDY_H
