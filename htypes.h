#ifndef HTYPES_H
#define HTYPES_H

typedef enum TPivot {
    FIRST,
    BEST,
}Pivot;

typedef enum TNeighbour {
    TRANSPOSE,
    EXCHANGE,
    INSERT
}Neighbour;

typedef enum TSolution {
    RANDOM,
    SIMPLIFIED
}Solution;

//misschien hier een betere naam voor verzinnen
typedef struct TIndexTuple {
    int index;
    float data;
} IndexTuple;

typedef struct TIterOptimResults {
    int sigma;
    int iterations;
    int meanresult;
} IterOptimResults;


typedef struct TAcoOptimResults {
    int numants;
    int iterations;
    double rho;
    double meanresult;
} AcoOptimResults;

#endif // HTYPES_H
