#include "vndbestsearch.h"

vector<int> VNDBestSearch::searchBest(vector<vector<int> > n, vector<int> current)
{
    vector<int> retval;
    long currentWCT = this->getInstance()->newComputeWCT(current);
    for(vector<int> v: n){
        long newWCT = this->getInstance()->newComputeWCT(v);
        if(newWCT < currentWCT){
            currentWCT = newWCT;
            retval = v;
        }
    }

    return retval;
}

VNDBestSearch::VNDBestSearch(PfspInstance *instance, InitialSolution* insol,bool changeup = false)
    :PivotRule(instance, NULL, insol)
{
    //maak een structuur met alle mogelijke neighbourhoods
    hoods.push_back(new TransposeNeighbourHood(instance));
    if(changeup) {
        hoods.push_back(new InsertNeighbourHood(instance));
        hoods.push_back(new ExchangeNeighbourHood(instance));
    } else {
        hoods.push_back(new ExchangeNeighbourHood(instance));
        hoods.push_back(new InsertNeighbourHood(instance));
    }

}

VNDBestSearch::~VNDBestSearch()
{
    for(NeighbourHood* n: hoods){
        delete n;
    }
}

vector<int> VNDBestSearch::searchForSolution(int timelimit)
{
    unsigned int k = hoods.size();
    unsigned int i = 0;

    long timenow = time(NULL);
    vector<int> pi = this->getInitialSolution()->generateInitialSolution();

    while(i < k) {
        if(timelimit > 0) {
            long delta_time = time(NULL) - timenow;
            if(ceil(delta_time/60) > timelimit)
                break;
        }
        vector<vector<int>> ni = hoods[i]->generateNeighbourHood(pi);
        vector<int> sol = searchBest(ni,pi);
        if(sol.empty()) {
            i++;
        } else {
            pi = sol;
            i = 0;
        }
    }

    return pi;
}
