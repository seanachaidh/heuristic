/***************************************************************************
 *   Copyright (C) 2012 by Jérémie Dubois-Lacoste   *
 *   jeremie.dl@gmail.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include <iostream>
#include <fstream>
#include <cmath>
#include <cstring>
#include "pfspinstance.h"


using namespace std;

PfspInstance::PfspInstance()
{
}

PfspInstance::PfspInstance(vector<vector<long int> > matrix, vector<int> priority)
{
    this->processingTimesMatrix.assign(matrix.begin(),matrix.end());
    this->priority.assign(priority.begin(),priority.end());
    this->nbJob  = matrix.size();
    this->nbMac = matrix[1].size();
}


PfspInstance::~PfspInstance()
{
}

int PfspInstance::getNbJob()
{
  return nbJob;
}

int PfspInstance::getNbMac()
{
  return nbMac;
}

/* Allow the memory for the processing times matrix : */
void PfspInstance::allowMatrixMemory(int nbJ, int nbM)
{
  processingTimesMatrix.resize(nbJ);          // 1st dimension

  for (int cpt = 0; cpt < nbJ; ++cpt)
    processingTimesMatrix[cpt].resize(nbM); // 2nd dimension

    dueDates.resize(nbJ);
    priority.resize(nbJ);
}


long int PfspInstance::getTime(int job, int machine)
{
    if ((job < 0) || (job > nbJob) || (machine < 0) || (machine > nbMac))
          cout    << "ERROR. file:pfspInstance.cpp, method:getTime. Out of bound. job=" << job
              << ", machine=" << machine << std::endl;

    return processingTimesMatrix[job][machine];
}

long PfspInstance::getPriority(int job)
{
    return this->priority[job];
}

void PfspInstance::setPriority(int job, int value)
{
    this->priority[job] = value;
}


/* Read the instance from file : */
bool PfspInstance::readDataFromFile(const char *fileName)
{
	bool everythingOK = true;
	int j, m; // iterators
	long int readValue;
	string str;
	ifstream fileIn;
    const char * aux2;
	char fileNameOK[100] = "";

	aux2 = (strrchr(fileName, '/'));

	if (aux2 == NULL)
		aux2 = fileName;
	else
		aux2 += 1;

	strcat(fileNameOK, aux2);

	cout << "name : " << fileNameOK << endl;
	cout << "file : " << fileName << endl;

	fileIn.open(fileName);

	if ( fileIn.is_open() ) {
        cout << "File " << fileName << " is now open, start to read..." << std::endl;

		fileIn >> nbJob;
        cout << "Number of jobs : " << nbJob << std::endl;
		fileIn >> nbMac;
        cout << "Number of machines : " << nbMac << std::endl;
        cout << "Allow memory for the matrix..." << std::endl;
		allowMatrixMemory(nbJob, nbMac);
        cout << "Memory allowed." << std::endl;
        cout << "Start to read matrix..." << std::endl;

        for (j = 0; j < nbJob; ++j)
		{
            for (m = 0; m < nbMac; ++m)
			{
				fileIn >> readValue; // The number of each machine, not important !
				fileIn >> readValue; // Process Time

				processingTimesMatrix[j][m] = readValue;
			}
		}
        fileIn >> str; // this is not read

        for (j = 0; j < nbJob; ++j)
		{
			fileIn >> readValue; // -1
			fileIn >> readValue;
			dueDates[j] = readValue;
			fileIn >> readValue; // -1
			fileIn >> readValue;
            priority[j] = readValue;
		}

        cout << "All is read from file." << std::endl;
		fileIn.close();
	}
	else
	{
		cout    << "ERROR. file:pfspInstance.cpp, method:readDataFromFile, "
				<< "error while opening file " << fileName << std::endl;

		everythingOK = false;
	}

	return everythingOK;
}

/*
 * Dit herhaaldelijk gebruiken gaat waarschijnlijk resulteren in een te traag programma
 *
 * We kunnen dit echter misschien gebruiken voor het maken van een partial solution, waarbij we dit gebruiken
 * voor de WTC coëfficienten van van de gedeeltelijke oplossigen van de RZ heuristiek.
 *
 * PS: Dit moet ik eens opnieuw implementeren
 */
/* Compute the weighted tardiness of a given solution */
long int PfspInstance::computeWCT(vector< int > & sol)
{
    unsigned int j;
    int m;
	int jobNumber;
	long int wct;
    unsigned int siz = sol.size();

	/* We need end times on previous machine : */
    vector< long int > previousMachineEndTime ( siz + 1 );
	/* And the end time of the previous job, on the same machine : */
	long int previousJobEndTime;

	/* 1st machine : */
	previousMachineEndTime[0] = 0;
    for ( j = 0; j < siz; ++j )
	{
		jobNumber = sol[j];
        previousMachineEndTime[j] = previousMachineEndTime[j] + processingTimesMatrix[jobNumber][1];
	}

	/* others machines : */
    for ( m = 1; m < nbMac; ++m )
	{
		previousMachineEndTime[1] +=
				processingTimesMatrix[sol[1]][m];
		previousJobEndTime = previousMachineEndTime[1];


        for ( j = 1; j < siz; ++j )
		{
			jobNumber = sol[j];

			if ( previousMachineEndTime[j] > previousJobEndTime )
			{
				previousMachineEndTime[j] = previousMachineEndTime[j] + processingTimesMatrix[jobNumber][m];
				previousJobEndTime = previousMachineEndTime[j];
			}
			else
			{
				previousJobEndTime += processingTimesMatrix[jobNumber][m];
				previousMachineEndTime[j] = previousJobEndTime;
			}
		}
	}

	wct = 0;
    for ( j = 0; j< siz; ++j )
	    wct += previousMachineEndTime[j] * priority[sol[j]];

    return wct;
}

long PfspInstance::newComputeWCT(vector<int> sol)
{
    vector<vector<long>> partialTime;
    vector<long> partialPrior;
    vector<vector<long>> result(sol.size(),vector<long>(nbMac));

    for(int i: sol){
        partialTime.push_back(processingTimesMatrix[i]);
        partialPrior.push_back(priority[i]);
    }
    for(unsigned int i = 0; i < sol.size(); i++) {
        for(int j = 0; j < nbMac; j++){
            if (i==0 && j == 0) {
                result[i][j] = partialTime[i][j];
            } else if(i == 0) {
                result[i][j] = partialTime[i][j] + result[i][j-1];
            } else if (j == 0){
                result[i][j] = partialTime[i][j] + result[i-1][j];
            } else {
                result[i][j] = partialTime[i][j] + max(result[i][j-1],result[i-1][j]);
            }
        }
    }
    //get the last row
    vector<long> lastRow(partialPrior.size());
    for(unsigned int i = 0; i < partialPrior.size(); i++){
        lastRow[i] = result[i].back();
    }
    long int retval = 0;
    for(unsigned int i = 0; i < lastRow.size(); i++) {
        retval+=partialPrior[i]*lastRow[i];
    }

    return retval;
}

/*
 * TODO: Dit moet ik eens verplaatsen gezien ik dit al ergens anders heb geïmplementeerd
 */
vector<int> PfspInstance::randomPermutation()
{
  vector<bool> alreadyTaken(nbJob, false); // nbJobs elements with value false
  vector<int > choosenNumber(nbJob, 0);
    vector <int> sol(nbJob);
  int nbj;
  int rnd, i, j, nbFalse;

  nbj = 0;
  for (i = nbJob; i >= 0; --i)
  {
    rnd = generateRndPosition(0, i);
    nbFalse = 0;

    /* find the rndth cell with value = false : */
    for (j = 0; nbFalse < rnd; ++j)
      if ( ! alreadyTaken[j] )
        ++nbFalse;
    --j;

    sol[j] = i;

    ++nbj;
    choosenNumber[nbj] = j;

    alreadyTaken[j] = true;
  }
  return sol;
}


