#include "vndfirstsearch.h"

vector<int> VNDFirstSearch::findFirstImprovement(vector<vector<int> > neighbours, vector<int> current)
{
    long currentWCT = this->getInstance()->newComputeWCT(current);
    for(vector<int> v: neighbours) {
        long newCurrentWCT = this->getInstance()->newComputeWCT(v);
        if (newCurrentWCT < currentWCT) {
            return v;
        }
    }

    return {};
}

vector<vector<int> > VNDFirstSearch::generatePartialNeighbourhood(vector<int> current, int fromwhere, int hood)
{
    if(fromwhere == 0)
        return this->hoods[hood]->generateNeighbourHood(current);

    vector<int> part(current.begin(),current.begin()+fromwhere);
    vector<int> tocheck(current.begin()+fromwhere, current.end());
    vector<vector<int>> retval;
    vector<vector<int>> nbourtocheck = hoods[hood]->generateNeighbourHood(tocheck);

    for(vector<int> nbour: nbourtocheck){
        nbour.insert(nbour.begin(),part.begin(),part.end());
        retval.push_back(nbour);
    }

    return retval;
}

VNDFirstSearch::VNDFirstSearch(PfspInstance *instance, InitialSolution *initsol, bool changeup = false)
    :PivotRule(instance,NULL,initsol) // Ik moet oppassen met het feit dat ik hier NULL meegeef
{
    //maak een structuur met alle mogelijke neighbourhoods
    hoods.push_back(new TransposeNeighbourHood(instance));
    if(changeup) {
        hoods.push_back(new InsertNeighbourHood(instance));
        hoods.push_back(new ExchangeNeighbourHood(instance));
    } else {
        hoods.push_back(new ExchangeNeighbourHood(instance));
        hoods.push_back(new InsertNeighbourHood(instance));
    }
}

VNDFirstSearch::~VNDFirstSearch()
{
    for(NeighbourHood* n: hoods){
        delete n;
    }
}

vector<int> VNDFirstSearch::searchForSolution(int timelimit)
{
    unsigned int k = hoods.size();
    unsigned int i = 0;

    long timenow = time(NULL);

    vector<int> pi = this->getInitialSolution()->generateInitialSolution();
    for(;;){
        for(int j = 0; j < this->getInstance()->getNbJob(); j++){
            while(i < k) {
                if(timelimit > 0) {
                    long delta_time = time(NULL) - timenow;
                    if(ceil(delta_time/60) > timelimit)
                        break;
                }

                vector<vector<int>> ni = generatePartialNeighbourhood(pi,j,i);
                vector<int> sol = findFirstImprovement(ni,pi);
                if(sol.empty()) {
                    i++;
                } else {
                    pi = sol;
                    i = 0;
                }

            }
        }
        unsigned int check_i = 0;
        while(check_i < k){
            vector<vector<int>> check_neigh = hoods[check_i]->generateNeighbourHood(pi);
            vector<int> check_optim = findFirstImprovement(check_neigh,pi);
            if(check_optim.empty()){
                check_i++;
            } else {
                pi = check_optim;
                //Niet vergeten resettten
                i = 0;
                break;
            }

        }

        if(check_i == k)
            break;
    }

    return pi;
}
